from TicTacToeBoard import TicTacToeBoard

class TicTacToePlayer:
    def __init__(self, board, value):
        self.board = board
        self.value = value

    def make_move(self):
        row,col = self.choose_move()
        self.board.play_square(row,col,self.value)

    def choose_move(self):
        pass

class TicTacToeHuman(TicTacToePlayer):
    def __init__(self, board, value):
        TicTacToePlayer.__init__(self, board, value)

    def choose_move(self):
        
        valid_move = False

        while not valid_move:
            print("your move, pick a row (0-2)")
            row = int(raw_input())
            print("your move, pick a col (0-2)")
            col = int(raw_input())

            if(self.board.get_square(row,col)!='N'):
                print("square already taken!")
            else:
                valid_move = True
                
        return (row,col)

class TicTacToeAI(TicTacToePlayer):
    def __init__(self, board, value):
        TicTacToePlayer.__init__(self, board, value)
        
    def choose_move(self):
        # Selects a move using alpha-beta pruning
        board_cp = self.board
        move = self.alphabeta(board_cp)
        return move

    def enumerate_moves(self, board):
        # Enumerates all possible moves given a game board
        possible_moves = [(i,j) 
                          for i in range(3) 
                          for j in range(3) 
                          if board.get_square(i,j) == 'N']
        return possible_moves

    def alphabeta(self, board):
        # Returns the best move, chosen via alpha-beta pruning
        
        best_move = None
        best_val = -float('Inf')
        possible_moves = self.enumerate_moves(board)

        # Initalize alpha and beta to the worst case
        alpha = -1
        beta = 1

        # For every possible move...
        for row,col in possible_moves:
            # If the best value found so far is a win, then return
            if best_val == 1:
                break
                
            # Otherwise, evaluate the minimax outcome of this move
            board.play_square(row,col,self.value)
            max_min_val = self.min_outcome(board, alpha, beta)
            board.play_square(row,col,'N')

            # Update the best value so far if necessary
            if max_min_val > best_val:
                best_move = (row,col)
                best_val = max_min_val

        return best_move

    def get_outcome(self,board,eval_fn, alpha, beta):
        # If eval_fn is max, return the max outcome; if eval_fn is min, return the min outcome.

        board.checks += 1
        
        # Recursive stopping condition: if the board is full or a player has already won, return the outcome
        winner = board.winner()
        if board.full_board() or winner != 'N':

            if winner == 'N':
                return 0
            elif winner == self.value:
                return 1
            else:
                return -1
        else:

            # All values that are different between max and min are assigned here:
            if eval_fn is max:
                new_fn = lambda x, new_alpha: self.min_outcome(x, new_alpha, beta)
                val_to_assign = self.value
                best_val = alpha
                best_possible_val = 1
            elif eval_fn is min:
                new_fn = lambda x, new_beta: self.max_outcome(x, alpha, new_beta)
                val_to_assign = self.opponent_value()
                best_val = beta
                best_possible_val = -1

                
            # For every possible move...
            possible_moves = self.enumerate_moves(board)
            for row,col in possible_moves:
                # If you know that this branch has the best outcome, return
                if best_val == best_possible_val:
                    break
                    
                # Otherwise get the outcome of a move
                board.play_square(row,col,val_to_assign)
                outcome = new_fn(board, best_val)

                board.play_square(row,col,'N')
                best_val = eval_fn(outcome, best_val)

            return best_val

    def max_outcome(self, board, alpha, beta):
        return self.get_outcome(board, max, alpha, beta)

    def min_outcome(self,board, alpha, beta):
        return self.get_outcome(board, min, alpha, beta)

    def opponent_value(self):
        # Returns the opponent's marker
        if self.value == 'X':
            return 'O'
        else:
            return 'X'
        

class TicTacToeMinimax(TicTacToeAI):
# Same as TicTacToeAI, but implements minimax instead of alpha-beta pruning
    def __init__(self, board, value):
        TicTacToeAI.__init__(self, board, value)

    def choose_move(self):
        # Selects a move using minimax
        board_cp = self.board
        move = self.minimax(board_cp)
        return move
    
    def minimax(self, board):
        # Returns the minimax move given a game board
        best_move = None
        best_val = -float('Inf')
        possible_moves = self.enumerate_moves(board)

        for row,col in possible_moves:
            board.play_square(row,col,self.value)
            max_min_val = self.min_outcome(board)
            board.play_square(row,col,'N')
            
            if max_min_val > best_val:
                best_move = (row,col)
                best_val = max_min_val

        return best_move

    def get_outcome(self,board,eval_fn):
        winner = board.winner()
        
        if board.full_board() or winner != 'N':
            if winner == 'N':
                return 0
            elif winner == self.value:
                return 1
            else:
                return -1
        else:
            if eval_fn is max:
                new_fn = self.min_outcome
                val_to_assign = self.value
                best_val = -1
            elif eval_fn is min:
                new_fn = self.max_outcome
                val_to_assign = self.opponent_value()
                best_val = 1

            possible_moves = self.enumerate_moves(board)

            for row,col in possible_moves:
                board.play_square(row,col,val_to_assign)
                outcome = new_fn(board)

                board.play_square(row,col,'N')
                best_val = eval_fn(outcome, best_val)

            return best_val

    def max_outcome(self, board):
        return self.get_outcome(board, max)

    def min_outcome(self,board):
        return self.get_outcome(board, min)
