import struct, string
from TicTacToeBoard import TicTacToeBoard
from TicTacToePlayer import TicTacToeHuman, TicTacToeAI


def play():
    Board = TicTacToeBoard()
    
    print '***** Play Tic Tac Toe! *****'
    setup = False

    while not setup:
        print 'Player X: (H)uman or (A)I?'

        response = str(raw_input())
        if response in ['Human','H','1']:
            playerX = TicTacToeHuman(Board, 'X')
            setup = True
        elif response in ['AI','A','2']:
            playerX = TicTacToeAI(Board, 'X')
            setup = True
        else:
            print 'Error: Did not understand input'
            
    setup = False
    while not setup:
        print 'Player O: (H)uman or (A)I?'
        response = str(raw_input())
        if response in ['Human','H','1']:
            playerO = TicTacToeHuman(Board, 'O')
            setup = True
        elif response in ['AI','A','2']:
            playerO = TicTacToeAI(Board, 'O')
            setup = True
        else:
            print 'Error: Did not understand input'

    while( Board.full_board() == False and Board.winner() == 'N'):
        print 'Player X to move:'
        Board.PrintBoard()
        playerX.make_move()

        if(Board.full_board() or Board.winner()!='N'):
            break
        
        print 'Player O to move:'
        Board.PrintBoard()
        playerO.make_move()

        if(Board.full_board() or Board.winner()!='N'):
            break
        
        # Print number of consistency checks
        print "%d Checks" %( Board.checks )
        
        Board.checks = 0

    Board.PrintBoard()

    if(Board.winner()=='N'):
        print("Cat game")
    elif(Board.winner()=='X'):
        print("Player X Wins!")
    elif(Board.winner()=='O'):
        print("Player O Wins!")
    print "%d Checks" %Board.checks

def main():
    play()

if __name__ == '__main__':
    main()
            
