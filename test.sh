#!/bin/bash

WINA=0
WINB=0
DRAW=0

NAMEA=${1:0:-3}
NAMEB=${2:0:-3}

echo Running $3 games...
for a in `seq 1 $3`;
do
    output=$( python3.3 Matchmaker.py $1 $2 | tail -1 )
    echo $output
    case "$output" in
	$NAMEA*) ((WINA++));;
	$NAMEB*) ((WINB++));;
	*) ((DRAW++))
    esac
done

echo
echo $NAMEA Wins: $WINA
echo $NAMEB Wins: $WINB
echo Draws  : $DRAW
